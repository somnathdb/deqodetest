const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const bcrypt = require("bcryptjs");
const SALT_I = 10;


const userSchema = new Schema({
    id:{
        type:String
    },
    password:{
        type:String
    },
    name:{
        type:String
    },
    age:{
        type:Number
    }
})

userSchema.pre("save", function(next) {
    var user = this;
    if (user.isModified("password")) {
      bcrypt.genSalt(SALT_I, function(err, salt) {
        if (err) {
          return next(err);
        }
        bcrypt.hash(user.password, salt, function(err, hash) {
          if (err) {
            return next(err);
          }
          user.password = hash;
          next();
        });
      });
    } else {
      next();
    }
  });

userSchema.methods.change_password = async (candidatePassword) => {
    //Creating a Hash
    var salt = bcrypt.genSaltSync(SALT_I);
    var hash = bcrypt.hashSync(candidatePassword, salt);
    return hash;
  };

module.exports = mongoose.model('user',userSchema)