const userSchema = require('../model/user')

exports.addUser = async(req,res,next) => {
    try{
        const body = req.body;
       //console.log(body.password)
        var add_new_user = new userSchema({
            id:body.id,
            name:body.name,
            password:body.password,
            age:body.age
        }) 
        let newUser = await add_new_user.save()
        if(newUser){
            res.status(201).json({
                message:"User Successfully Added"
            })
        }
    }
    catch{
        res.status(500).json({
            message:"Error"
        })
    }
}