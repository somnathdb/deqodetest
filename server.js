const express = require('express')
const app = express()
const mongoose = require('mongoose')
const db = require('./config/keys').mongoURL
const bodyParser = require('body-parser')

mongoose.connect(db, {
    useNewUrlParser: true,
    useUnifiedTopology: true
}).then((req, res, next) => {
    console.log("Database Connect SuccessFully")
})



app.use(bodyParser.urlencoded({
    extended: false
}))

app.use(bodyParser.json())

const addUser = require('./server/routes/userRoute')
app.use('/user',addUser)


app.listen(5000, () => {
    console.log("Server Start Successfully")
})